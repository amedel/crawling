package com.marfeel.test.webCrawler.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.net.ssl.SSLHandshakeException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.marfeel.test.webCrawler.evaluator.MarfeelizableEvaluator;
import com.marfeel.test.webCrawler.mongodb.VisitedSite;
import com.marfeel.test.webCrawler.mongodb.repository.VisitedSiteRepository;
import com.marfeel.test.webCrawler.worker.DefaultCrawlWorker;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class CrawlWorkerTest {

	@Mock
	private VisitedSiteRepository visitedSiteRepository;
	
	@Mock
	private MarfeelizableEvaluator marfeelizableEvaluator;
	
	@Captor 
	ArgumentCaptor<VisitedSite> visitedSiteCaptor;
	
	@Mock
	Connection connection;
	@Mock
	Document document;
	
	DefaultCrawlWorker defaultCrawlWorker;

	
	@Test
	public void should_mark_site_as_marfeelizable() throws Exception {
		
        Mockito.when(connection.get()).thenReturn(document);
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect(Mockito.anyString())).thenReturn(connection);
        PowerMockito.when(marfeelizableEvaluator.isMarfeelizableSite(document)).thenReturn(true);
        
        defaultCrawlWorker =  new DefaultCrawlWorker("google.com", visitedSiteRepository, marfeelizableEvaluator);
        defaultCrawlWorker.call();
        
        Mockito.verify(visitedSiteRepository).save(visitedSiteCaptor.capture());
        assertTrue(visitedSiteCaptor.getValue().isMarfeelizable());

	}
	
	@Test
	public void should_mark_site_as_not_marfeelizable_because_exception() throws Exception {
		
		
        Mockito.when(connection.get()).thenThrow(new IOException());
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect(Mockito.anyString())).thenReturn(connection);
                
        defaultCrawlWorker =  new DefaultCrawlWorker("google.com", visitedSiteRepository, marfeelizableEvaluator);
        defaultCrawlWorker.call();
        
        Mockito.verify(visitedSiteRepository).save(visitedSiteCaptor.capture());
        assertFalse(visitedSiteCaptor.getValue().isMarfeelizable());

	}
	
	@Test
	public void should_try_https_if_http_fails() throws Exception {
		
        Mockito.when(connection.get()).thenThrow(new SSLHandshakeException("Certificate"));
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect(Mockito.anyString())).thenReturn(connection);
                
        defaultCrawlWorker =  new DefaultCrawlWorker("google.com", visitedSiteRepository, marfeelizableEvaluator);
        defaultCrawlWorker.call();
        
        PowerMockito.verifyStatic(Jsoup.class, Mockito.times(2));
        Jsoup.connect(Mockito.anyString());
        Mockito.verify(visitedSiteRepository).save(visitedSiteCaptor.capture());
        assertFalse(visitedSiteCaptor.getValue().isMarfeelizable());

	}

	
	@Test
	public void should_mark_site_marfeelizable_if_https_works() throws Exception {
		
		Connection failsConnection = PowerMockito.mock(Connection.class);
		Connection okConnection = PowerMockito.mock(Connection.class);
        Mockito.when(failsConnection.get()).thenThrow(new SSLHandshakeException("Certificate"));
        
        Mockito.when(okConnection.get()).thenReturn(document);
        PowerMockito.when(marfeelizableEvaluator.isMarfeelizableSite(document)).thenReturn(true);
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect("http://google.com")).thenReturn(failsConnection);
        PowerMockito.when(Jsoup.connect("https://google.com")).thenReturn(okConnection);
                
        defaultCrawlWorker =  new DefaultCrawlWorker("google.com", visitedSiteRepository, marfeelizableEvaluator);
        defaultCrawlWorker.call();
        
        PowerMockito.verifyStatic(Jsoup.class, Mockito.times(2));
        Jsoup.connect(Mockito.anyString());
        Mockito.verify(visitedSiteRepository).save(visitedSiteCaptor.capture());
        assertTrue(visitedSiteCaptor.getValue().isMarfeelizable());

	}
}
