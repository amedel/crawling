package com.marfeel.test.webCrawler.mongodb;

import java.time.Instant;

import org.springframework.data.annotation.Id;

public class VisitedSite {

	@Id
	private String id;
	
	
	private String url;
	private boolean isMarfeelizable;
	private Instant timestamp;

	public VisitedSite(String id, String url, boolean isMarfeelizable, Instant timestamp) {
		this.id=id;
		this.url = url;
		this.isMarfeelizable = isMarfeelizable;
		this.timestamp = timestamp;
	}
	
	public String getUrl() {
		return url;
	}
	
	public boolean isMarfeelizable() {
		return isMarfeelizable;
	}
	
	public Instant getTimestamp() {
		return timestamp;
	}
	
	public String getId() {
		return id;
	}	
}
