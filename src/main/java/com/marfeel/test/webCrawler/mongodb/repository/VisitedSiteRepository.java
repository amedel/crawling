package com.marfeel.test.webCrawler.mongodb.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.marfeel.test.webCrawler.mongodb.VisitedSite;

public interface VisitedSiteRepository extends CrudRepository<VisitedSite, String> {
	
	public List<VisitedSite> findByUrl(String urlToSearch);
	
}
