package com.marfeel.test.webCrawler.worker.factory;

import com.marfeel.test.webCrawler.evaluator.factory.MarfeelizableEvaluatorFactory;
import com.marfeel.test.webCrawler.mongodb.repository.VisitedSiteRepository;
import com.marfeel.test.webCrawler.worker.CrawlWorker;
import com.marfeel.test.webCrawler.worker.DefaultCrawlWorker;

public class DefaultCrawlWorkerFactory implements CrawlWorkerFactory {

	private final VisitedSiteRepository visitedSiteRepository;
	private final MarfeelizableEvaluatorFactory marfeelizableEvaluatorFactory;

	public DefaultCrawlWorkerFactory(VisitedSiteRepository visitedSiteRepository,
			MarfeelizableEvaluatorFactory marfeelizableEvaluatorFactory) {
		super();
		this.visitedSiteRepository = visitedSiteRepository;
		this.marfeelizableEvaluatorFactory = marfeelizableEvaluatorFactory;
	}

	@Override
	public CrawlWorker create(String url) {
		return new DefaultCrawlWorker(url, visitedSiteRepository, marfeelizableEvaluatorFactory.create());
	}
	
}
