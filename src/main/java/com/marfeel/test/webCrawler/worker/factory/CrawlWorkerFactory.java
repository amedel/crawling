package com.marfeel.test.webCrawler.worker.factory;

import com.marfeel.test.webCrawler.worker.CrawlWorker;

public interface CrawlWorkerFactory {

	public CrawlWorker create(String url);
}
