package com.marfeel.test.webCrawler.criteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DefaultMarfeelizableCriteriaListFactory implements MarfeelizableCriteriaListFactory {
	
	private final String RESOURCE_SPLIT_CHARACTER = ",";
	private final List<String> marfeelizableWords;
	
	public DefaultMarfeelizableCriteriaListFactory(String marfeelizableWordsString) {
		super();
		this.marfeelizableWords = Arrays.asList(marfeelizableWordsString.split(RESOURCE_SPLIT_CHARACTER));
	}

	@Override
	public List<MarfeelizableCriteria> getCriteriaList() {
		List<MarfeelizableCriteria> marfeelizableCriteriaList = new ArrayList<>(1);
		marfeelizableCriteriaList.add(new TitleMarfeelizableCriteria(marfeelizableWords));
		return marfeelizableCriteriaList;
	}

}
