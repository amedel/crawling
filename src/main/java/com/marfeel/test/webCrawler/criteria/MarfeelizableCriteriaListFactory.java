package com.marfeel.test.webCrawler.criteria;

import java.util.List;

public interface MarfeelizableCriteriaListFactory {

	List<MarfeelizableCriteria> getCriteriaList();
	
}
