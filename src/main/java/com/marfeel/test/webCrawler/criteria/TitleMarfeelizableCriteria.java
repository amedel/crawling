package com.marfeel.test.webCrawler.criteria;

import java.util.List;

import org.jsoup.nodes.Document;

public class TitleMarfeelizableCriteria implements MarfeelizableCriteria {
	
	private final List<String> marfeelizableWords;
	
	public TitleMarfeelizableCriteria(List<String> marfeelizableWords) {
		this.marfeelizableWords = marfeelizableWords;
	}

	@Override
	public boolean isMarfeelizable(Document document) {
		boolean result = false;
		if(document!=null && document.title()!=null) {
			final String title = document.title();
			result = marfeelizableWords.stream().anyMatch(title::contains);
		}
		return result;
	}

}
