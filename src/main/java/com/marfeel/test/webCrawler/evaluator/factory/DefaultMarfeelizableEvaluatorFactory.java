package com.marfeel.test.webCrawler.evaluator.factory;

import com.marfeel.test.webCrawler.criteria.MarfeelizableCriteriaListFactory;
import com.marfeel.test.webCrawler.evaluator.MarfeelizableEvaluator;

public class DefaultMarfeelizableEvaluatorFactory implements MarfeelizableEvaluatorFactory {

	private MarfeelizableCriteriaListFactory marfeelizableCriteriaListFactory;
	
	public DefaultMarfeelizableEvaluatorFactory(MarfeelizableCriteriaListFactory marfeelizableCriteriaListFactory) {
		super();
		this.marfeelizableCriteriaListFactory = marfeelizableCriteriaListFactory;
	}

	@Override
	public MarfeelizableEvaluator create() {
		return new MarfeelizableEvaluator(marfeelizableCriteriaListFactory.getCriteriaList());
	}

}
