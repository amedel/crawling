package com.marfeel.test.webCrawler.evaluator;

import java.util.List;

import org.jsoup.nodes.Document;

import com.marfeel.test.webCrawler.criteria.MarfeelizableCriteria;

public class MarfeelizableEvaluator {

	final List<MarfeelizableCriteria> marfeelizableCriteriaList;
	
	public MarfeelizableEvaluator(List<MarfeelizableCriteria> marfeelizableCriteriaList) {
		super();
		this.marfeelizableCriteriaList = marfeelizableCriteriaList;
	}

	public boolean isMarfeelizableSite(Document document) {
		return marfeelizableCriteriaList.stream()
		                         .anyMatch(criteria->criteria.isMarfeelizable(document));
	}
	
}
