package com.marfeel.test.webCrawler.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.marfeel.test.webCrawler.criteria.DefaultMarfeelizableCriteriaListFactory;
import com.marfeel.test.webCrawler.criteria.MarfeelizableCriteriaListFactory;
import com.marfeel.test.webCrawler.evaluator.factory.DefaultMarfeelizableEvaluatorFactory;
import com.marfeel.test.webCrawler.evaluator.factory.MarfeelizableEvaluatorFactory;
import com.marfeel.test.webCrawler.launcher.CrawlLauncher;
import com.marfeel.test.webCrawler.launcher.DefaultCrawlLauncher;
import com.marfeel.test.webCrawler.mongodb.repository.VisitedSiteRepository;
import com.marfeel.test.webCrawler.worker.factory.CrawlWorkerFactory;
import com.marfeel.test.webCrawler.worker.factory.DefaultCrawlWorkerFactory;

@Configuration
@PropertySource("classpath:webCrawler.properties")
@ComponentScan(basePackages="com.marfeel.test.webCrawler")
@EnableWebMvc
public class MvcConfiguration implements WebMvcConfigurer {
	
	@Value("${webcrawler.marfeelizable.words}")
	private String marfeelizableWordsString;
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	        converters.add(new MappingJackson2HttpMessageConverter());
	}
	
	@Bean
	public CrawlLauncher crawlLauncher() {
		return new DefaultCrawlLauncher(); 
	}
	
	@Bean
	public CrawlWorkerFactory crawlWorkerFactory(VisitedSiteRepository visitedSiteRepository,
			MarfeelizableEvaluatorFactory marfeelizableEvaluatorFactory) {
		return new DefaultCrawlWorkerFactory(visitedSiteRepository, marfeelizableEvaluatorFactory);
	}
	
	@Bean
	public MarfeelizableCriteriaListFactory marfeelizableCriteriaListFactory() {
		return new DefaultMarfeelizableCriteriaListFactory(marfeelizableWordsString); 
	}
	
	@Bean
	public MarfeelizableEvaluatorFactory marfeelizableEvaluatorFactory(MarfeelizableCriteriaListFactory marfeelizableCriteriaListFactory) {
		return new DefaultMarfeelizableEvaluatorFactory(marfeelizableCriteriaListFactory); 
	}
	
}
